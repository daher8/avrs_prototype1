# This script is an example of how an aviation focused
# speech recognition software would work. sample.py was
# created for the Daher 2019 Aviathon contest by Team 8.
# Authors: Ben Krosner, Adam Kaplan, September 2019

import azure.cognitiveservices.speech as speechsdk
import re, string

# defining useful dictionaries that will allow us to take advantage of the
# limited aviation lexicon to parse text into meaningful information
phoneAlph = {
    "ALPHA" : "A",
    "BRAVO" : "B",
    "CHARLIE" : "C",
    "DELTA" : "D",
    "ECHO" : "E",
    "FOXTROT" : "F",
    "FOX" : "F",
    "GOLF" : "G",
    "HOTEL" : "H",
    "INDIA" : "I",
    "JULIETT" : "J",
    "KILO" : "K",
    "LIMA" : "L",
    "MIKE" : "M",
    "NOVEMBER" : "N",
    "OSCAR" : "O",
    "PAPA" : "P",
    "QUEBEC" : "Q",
    "ROMEO" : "R",
    "SIERRA" : "S",
    "TANGO" : "T",
    "UNIFORM" : "U",
    "VICTOR" : "V",
    "WHISKEY" : "W",
    "X-RAY" : "X",
    "YANKEE" : "Y",
    "ZULU" : "Z",
}

numbers = {
    "ZERO" : "0",
    "ONE" : "1",
    "TWO" : "2",
    "THREE" : "3",
    "FOUR" : "4",
    "FIVE" : "5",
    "SIX" : "6",
    "SEVEN" : "7",
    "EIGHT" : "8",
    "NINE" : "9",

}

aviationTerms = {
    "FLIGHT LEVEL" : "FL",
    "DEPARTURE FREQUENCY" : "DEP FREQ"
}

tailNumber = {

    "N8KF" : "N8KF",
    "TBM8KF" : "N8KF",
    "TBM 8KF" : "N8KF"

}

time = {
    "MINUTES" : "MINUTES",
    "MINUTE" : "MINUTE"
}

# Creates an instance of a speech config with specified subscription key and service region.
# Replace with your own subscription key and service region (e.g., "westus").
speech_key, service_region = "<insert-key-kere>", "westus"
speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region)

# Creates a recognizer with the given settings, using Azure services for now
speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)

print("Say something...")


# Starts speech recognition, and returns after a single utterance is recognized. The end of a
# single utterance is determined by listening for silence at the end or until a maximum of 15
# seconds of audio is processed.  The task returns the recognition text as result. 
# Note: Since recognize_once() returns only a single utterance, it is suitable only for single
# shot recognition like command or query. 
# For long-running multi-utterance recognition, use start_continuous_recognition() instead.
result = speech_recognizer.recognize_once()

# Checks result.
num = False
if result.reason == speechsdk.ResultReason.RecognizedSpeech:
    print("Recognized: {}".format(result.text))
    # below shows one way we can post-process text with aviation knowledge
    str_0 = format(result.text)
    str_0 = str_0[:-1]
    str_0 = str_0.translate(str.maketrans('', '', string.punctuation))
    m=re.compile(" ").split(str_0)
    str2 = ''
    list = []
    for x in m:
        if x.upper() in phoneAlph:
            str2 += phoneAlph[x.upper()]
            list.append(phoneAlph[x.upper()])
            num = False
        elif x.upper() in numbers:
            if num or str2[-2].isnumeric():
                #print(str2[-1])
                str2 = str2[:-1]
                list[-1] += numbers[x.upper()]
            else: 
                list.append(numbers[x.upper()])
            str2 += numbers[x.upper()]
            
            num = True
        else:
            str2 += x.upper()
            list.append(x.upper())
            num = False
        str2 += ' '
    str_last = ''
    for x in range(len(list)-1):
        str3 = list[x]
        str4 = list[x+1]
        str5 = str3 + ' ' + str4
        if str5 in aviationTerms:
            str_last += aviationTerms[str5]
            list[x+1] = ''
        elif str4 in time:
            if str3.isnumeric():
                if int(str3)>100:
                    num1 = round(int(str3)/100)
                    num2 = int(str3)-num1*100
                    numstr = str(num1) + ' ' + str(num2) + ' '
                    str_last += numstr
                else:
                    str_last += str3
                    
        elif x==len(list)-2:
            str_last += str3 + ' ' + str4
        elif str3.isnumeric() and str4.isnumeric():
            num = int(str3)*10**len(str4)+int(str4)
            num = float(num)/100
            str_last += str(num)
            list[x+1] = ''
        else:
            str_last += str3 + ' '
    print(str_last)
elif result.reason == speechsdk.ResultReason.NoMatch:
    print("No speech could be recognized: {}".format(result.no_match_details))
elif result.reason == speechsdk.ResultReason.Canceled:
    cancellation_details = result.cancellation_details
    print("Speech Recognition canceled: {}".format(cancellation_details.reason))
    if cancellation_details.reason == speechsdk.CancellationReason.Error:
        print("Error details: {}".format(cancellation_details.error_details))
        